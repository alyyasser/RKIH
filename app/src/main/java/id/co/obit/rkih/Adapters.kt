package id.co.obit.rkih

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

/**
 * Created by NgekNgok
 */
object Adapters {

	@JvmStatic
	@BindingAdapter("iconPath")
	fun loadImage(view: ImageView, iconPath: String) {
		try {
			FirebaseStorage.getInstance().reference.child(iconPath).downloadUrl
					.addOnSuccessListener { uri -> Picasso.with(view.context).load(uri.toString()).placeholder(R.mipmap.app_icon_default).error(R.mipmap.app_icon_default).into(view) }
					.addOnFailureListener { Picasso.with(view.context).load(R.mipmap.app_icon_default).into(view) }
		} catch (e: IllegalArgumentException) {
			Picasso.with(view.context).load(R.mipmap.app_icon_default).into(view)
		}
	}

}