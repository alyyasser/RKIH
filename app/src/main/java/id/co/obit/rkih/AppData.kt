package id.co.obit.rkih

import android.content.ActivityNotFoundException
import android.content.Intent
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.net.Uri
import android.view.View
import id.co.obit.rkih.util.snackbar

/**
 * Created by NgekNgok
 */

data class AppData(val id: String, val title: String, val iconPath: String, val storeAvailability: Boolean) : BaseObservable() {

	@get:Bindable
	var installed: Boolean = false
		set(value) {
			field = value
			notifyPropertyChanged(BR.installed)
		}

	fun PrimaryBtnClick(v: View) {
		val ctx = v.context
		if (installed) {
			ctx.startActivity(ctx.packageManager.getLaunchIntentForPackage(id))
		} else {
			try {
				ctx.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$id")))
			} catch (e: ActivityNotFoundException) {
				ctx.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$id")))
			}
		}
	}

	fun SecondaryBtnClick(v: View) {
		//TODO: implementation for "more info about app"
		v.snackbar(R.string.txt_not_implemented)
	}

}