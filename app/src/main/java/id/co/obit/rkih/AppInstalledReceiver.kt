package id.co.obit.rkih

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/**
 * Created by NgekNgok
 */
class AppInstalledReceiver : BroadcastReceiver() {
	override fun onReceive(context: Context?, intent: Intent?) {
		val packageName: String = intent!!.data.encodedSchemeSpecificPart
		val action: String = intent!!.action
		if (packageName.isNullOrEmpty()) return

		for (app in Data.apps) {
			if (app.id == packageName && action.equals(Intent.ACTION_PACKAGE_ADDED)) app.installed = true
			if (app.id == packageName && action.equals(Intent.ACTION_PACKAGE_REMOVED)) app.installed = false
			if (app.id == packageName && action.equals(Intent.ACTION_PACKAGE_FULLY_REMOVED)) app.installed = false
		}
	}
}