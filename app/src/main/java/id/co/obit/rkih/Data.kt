package id.co.obit.rkih

import android.databinding.ObservableArrayList

/**
 * Created by NgekNgok
 */
object Data {
	val apps = ObservableArrayList<AppData>().apply {
		add(AppData("com.aplikasi.adlee.sipomas", "SIPOMAS - Polres Kotabaru", "app-icon/sipomas_ktb.png", true))
		add(AppData("id.co.obit.sikomar", "SIKOMAR", "", false))
	}
}