package id.co.obit.rkih

import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.ViewGroup
import id.co.obit.rkih.databinding.ActivityMainBinding
import id.co.obit.rkih.databinding.AppRowBinding
import org.jetbrains.anko.startActivityForResult

/**
 * Created by NgekNgok
 */
class MainActivity : AppCompatActivity() {

	val binding: ActivityMainBinding by lazy {
		DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		for (app in Data.apps) {
			if (app.storeAvailability) {
				app.installed = isCallable(app.id)
			}
		}

		startActivityForResult<WelcomeActivity>(3190)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (resultCode != RESULT_OK) return

		binding.recyclerView.layoutManager = LinearLayoutManager(this)
		binding.recyclerView.adapter = object : Adapter<ViewHolder>() {
			override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
				holder!!.binding.appdata = Data.apps[position]
			}

			override fun getItemCount(): Int {
				return Data.apps.size
			}

			override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
				val view = ViewHolder(AppRowBinding.inflate(layoutInflater, parent, false))
				return view
			}
		}
	}

	class ViewHolder(val binding: AppRowBinding): RecyclerView.ViewHolder(binding.root)

	private fun isCallable(name: String): Boolean {
		for (installedPackage in packageManager.getInstalledPackages(0)) {
			if (installedPackage.packageName.equals(name)) return true
		}
		return false
	}
}