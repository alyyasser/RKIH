package id.co.obit.rkih

import android.databinding.BaseObservable
import android.databinding.Bindable

/**
 * Created by NgekNgok
 */

class State : BaseObservable() {

	@get:Bindable
	var ready: Boolean = false
		set(value) {
			field = value
			notifyPropertyChanged(BR.ready)
		}
}