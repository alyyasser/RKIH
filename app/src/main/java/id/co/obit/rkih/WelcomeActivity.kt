package id.co.obit.rkih

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.co.obit.rkih.databinding.SplashScreenBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by NgekNgok
 */
class WelcomeActivity : AppCompatActivity() {

	val binding: SplashScreenBinding by lazy {
		DataBindingUtil.setContentView<SplashScreenBinding>(this, R.layout.splash_screen)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val state = State()
		binding.state = state

		doAsync {
			Thread.sleep(5000)
			uiThread {
				binding.state.ready = true
				doAsync {
					Thread.sleep(500)
					uiThread {
						setResult(RESULT_OK)
						finish()
					}
				}
			}
		}
	}

}